//
//  UIColorIRecruit.swift
//  ChallengeiOS
//
//  Created by Macbook Retina on 30/06/2016.
//  Copyright © 2016 Guilherme Pola. All rights reserved.
//

import UIKit


public extension UIColor {
   
    class public func blueNavigationBar() -> UIColor {
        
        let red: CGFloat = CGFloat(52.0/255.0)
        let green: CGFloat = CGFloat(171.0/255.0)
        let blue: CGFloat = CGFloat(230.0/255.0)

        
        return UIColor.init(red: red, green: green, blue: blue, alpha: 1.0)
    }

}