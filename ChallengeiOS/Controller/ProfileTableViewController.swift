//
//  ProfileTableViewController.swift
//  ChallengeiOS
//
//  Created by Macbook Retina on 28/06/2016.
//  Copyright © 2016 Guilherme Pola. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {

    
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    
    let arrayProfile = ProfileModel.attributeInfo()

    override func viewDidLoad() {
        super.viewDidLoad()

        configNavigation()
        configHeader()
        
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    //MARK: UITableViewDataSource and UITableViewDelegate
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
            return arrayProfile.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellProfile", forIndexPath: indexPath)
           
            return cell

        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cellInfo", forIndexPath: indexPath) as! OptionsTableViewCell
            
            cell.setModel(arrayProfile[indexPath.row])
  
            let red = CGFloat((69.0 - (7.0 * Double(indexPath.row))) / 255.0)
            let green = CGFloat((175.0 - (16.0 * Double(indexPath.row))) / 255.0)
            let blue = CGFloat((231.0 - (21.0 * Double(indexPath.row))) / 255.0)
            
            cell.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)

            
            return cell

        }

    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 58
       
        }else{
          
            var heightScreen = view.frame.size.height
            heightScreen = heightScreen >= 568 ? (heightScreen - 370) / 4 : (568 - 370) / 4
            return heightScreen

    
        }
    }
    
    // MARK: Scroll view delegate
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 {
            // scrolling up
            containerView.clipsToBounds = true
            bottomSpaceConstraint?.constant = -scrollView.contentOffset.y / 2
            topSpaceConstraint?.constant = scrollView.contentOffset.y / 2
        } else {
            // scrolling down
            topSpaceConstraint?.constant = scrollView.contentOffset.y
            containerView.clipsToBounds = false
        }
    }
   
    // MARK: Config Header
    func configHeader(){
    
        imageViewCover.image = imageViewCover.image!.applyBlurWithRadius(20, tintColor: UIColor(white: 0.38, alpha: 0.38), saturationDeltaFactor: 1.8)
        imageViewProfile.layer.cornerRadius = 50.0
        imageViewProfile.layer.masksToBounds = true;
    
    }
    // MARK: Config Navigation

    func configNavigation(){
    
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.blueNavigationBar(),
            NSFontAttributeName: UIFont.init(name: "WhitneyHTF-Medium", size: 16)!]
    }

}
