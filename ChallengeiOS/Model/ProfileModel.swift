//
//  ProfileModel.swift
//  ChallengeiOS
//
//  Created by Macbook Retina on 29/06/2016.
//  Copyright © 2016 Guilherme Pola. All rights reserved.
//

import UIKit

class ProfileModel: NSObject {
    

    var stringTitle: String?
    var stringDescription: String?
    var stringImage: String?

    
  class func attributeInfo() -> [ProfileModel] {
        var arrayProfiles = [ProfileModel]()

    let arrayTitle = ["Personal Info", "Employment Info", "Digital CV", "Personality Test"]
    let arrayDescription = ["Name, photos, date of birth, blurb, and more.",
                            "Everything about the job you want.",
                            "Skills, experience, education, and more.",
                            "Tests to help match you even more accurately."]
    
    let arrayImage = ["personalInfo", "employmentInfo", "digitalCv", "personalityTest"]
    
        for var i in 0..<arrayTitle.count {
            let profile = ProfileModel()
            
            profile.stringTitle = arrayTitle[i]
            profile.stringDescription = arrayDescription[i]
            profile.stringImage = arrayImage[i]
            
            arrayProfiles.append(profile)
        }


        
        
        return arrayProfiles

    }
    

    

}
