//
//  OptionsTableViewCell.swift
//  ChallengeiOS
//
//  Created by Macbook Retina on 28/06/2016.
//  Copyright © 2016 Guilherme Pola. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }


    func setModel(model: ProfileModel){

        labelTitle.text = model.stringTitle
        labelDescription.text = model.stringDescription
        imageViewIcon.image = UIImage.init(named: model.stringImage!)

    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
